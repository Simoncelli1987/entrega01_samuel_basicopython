# Definicion de funciones de empresion de texto


def bienvenida():
    print ("""bienvenido a fdisk

los cambios quedara solo en la memoria hasta que decidas guardarlos;
ten cuidado antes de escribir comandos\n""")

def menu_opciones():
    print("""
Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table\n""")
    
def funcion_l():
    print ("""0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT\n""")

diccionario={"0":"Vacia","1":"FAR12", "2":"XENIX root", "3":"XENIX usr", "4":"FAT16<32M", "5":"Extendida", "6":"FAT16", "7":"HPFS/NTFexFAT", "8":"AIX", "9":"AIX arrancalbe", "10":"Gestor de arranque", 
"b":"W95 FAT32", "c":"W95 FAT32 (LBA)", "e":"W95 FAT16 (LBA)", "f":"W95 Ext'd (LVA)", "10":"OPUS", "11":"FAT12 oculta", "12":"compaq diagnost", "14":"FAT16 oculta<3",
"16":"FAT16 oculta", "17":"HPFS/NTFS oculta", "18":"SmartSleep", "1b":"FAT32 de W95 oculta", "1c":"FAT32"}
def funcion_b():
    print("""There is no *BSD partition on /dev/sda.

The device (null) does not contain BSD disklabel.\n""")

def funcion_F():
    print ("list free unpartitiones space\n")
    print ("""Unpartitioned space /dev/sda: 0 B, 0 bytes, 0 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes\n""")
    
def funcion_n():
    print("add a new partition\n")
    print ("""All space for primary partitions is in use.
Adding logical partition 6
No free sectors available.\n""")

def funcion_p():
    print ("print the partition table\n")
    print ("""Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x01fa6a1c

Disposit.  Inicio   Start    Final Sectores  Size Id Tipo
/dev/sda1  *         2048   999423   997376  487M 83 Linux
/dev/sda2         1001470 20969471 19968002  9,5G  5 Extendida
/dev/sda5         1001472 20969471 19968000  9,5G 8e Linux LVM\n""")

def funcion_L():
    print ("""0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT\n""")

def funcion_Lelse():
     print ("""Type 0 means free space to many systems. Having partitions of type 0 is probably unwise.

Changed type of partition 'Linux LVM' to 'desconocido'.\n""")

def funcion_V():
    print("verify the partition table\n")
    print("Remaining 4095 unallocated 512-byte sectors.\n")

def funcion_i1():
     print ("""  Device: /dev/sda1
           Boot: *
          Start: 2048
            End: 999423
        Sectors: 997376
      Cylinders: 63
           Size: 487M
             Id: 83
           Type: Linux
    Start-C/H/S: 0/33/32
      End-C/H/S: 62/55/53
          Attrs: 80\n""")

def funcion_i2():
    print (""" Device: /dev/sda2
          Start: 1001470
            End: 20969471
        Sectors: 19968002
      Cylinders: 1243
           Size: 9,5G
             Id: 5
           Type: Extendida
    Start-C/H/S: 62/23/86
      End-C/H/S: 1023/63/254\n""")

def funcion_i5():
    print ("""Device: /dev/sda5
          Start: 1001472
            End: 20969471
        Sectors: 19968000
      Cylinders: 1243
           Size: 9,5G
             Id: 0
           Type: Vacía
    Start-C/H/S: 62/25/86
      End-C/H/S: 1023/63/254\n""")

def funcion_xm():
    print("""DOS (MBR)
   b   move beginning of data in a partition
   i   change the disk identifier

  Geometría
   c   change number of cylinders
   h   change number of heads
   s   change number of sectors/track

  Generic
   p   print the partition table
   v   verify the partition table
   d   print the raw data of the first sector from the device
   D   print the raw data of the disklabel from the device
   f   fix partitions order
   m   print this menu

  Save & Exit
   q   quit without saving changes
   r   return to main menu\n""")

def funcion_xp():
    print("print the partition table")
    print("""Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x01fa6a1c

Disposit.  Inicio   Start    Final Sectores Id Tipo      Start-C/H/S   End-C/H/S Attrs
/dev/sda1  *         2048   999423   997376 83 Linux         0/33/32    62/55/53    80
/dev/sda2         1001470 20969471 19968002  5 Extendida    62/23/86 1023/63/254
/dev/sda5         1001472 20969471 19968000 8e Linux LVM    62/25/86 1023/63/254\n""")

def funcion_xv():
    print ("verify the partition table\n")
    print("Remaining 4095 unallocated 512-byte sectors.\n")

def funcion_xd():
    print ("print the raw data of the prist sector from the device\n")
    print("""00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  1c 6a fa 01 00 00 80 20
000001c0  21 00 83 35 37 3e 00 08  00 00 00 38 0f 00 00 56
000001d0  17 3e 05 fe ff ff fe 47  0f 00 02 b0 30 01 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa\n""")

def funcion_xD():
    print("print the raw data of the disklabel from the device\n")
    print("""MBR: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  1c 6a fa 01 00 00 80 20
000001c0  21 00 83 35 37 3e 00 08  00 00 00 38 0f 00 00 56
000001d0  17 3e 05 fe ff ff fe 47  0f 00 02 b0 30 01 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa

EBR: offset = 512752640, size = 512 bytes.
1e8ffc00  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
1e8ffdb0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 56
1e8ffdc0  19 3e 8e fe ff ff 02 00  00 00 00 b0 30 01 00 00
1e8ffdd0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
1e8ffdf0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa\n""")

def funcion_I():
    print("load disk layout from sfdisk script file\n")
    script=input("enter script file name:")
    print("""Created a new DOS disklabel with disk identifier 0x01fa6a1c.
Created a new partition 1 of type 'Linux' and of size 487 MiB.
Created a new partition 2 of type 'Extended' and of size 9,5 GiB.
Created a new partition 5 of type 'Linux LVM' and of size 9,5 GiB.
Script successfully applied.\n""")

def funcion_O():
    print("dump disk layout to sfdisk script file\n")
    script1=input("enter script file name:")
    print(script1, "guardado satisfactoriamente\n")


def funcion_w():
    print("""write table to disk and exit.
The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Dispositivo o recurso ocupado

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).
\n""")


def funcion_quit():
    print("quit without saving changes\n")
    

    
        
#empezamos poñendo a introduccion do script
bienvenida()
#procedemos a escribir codigo
while True:         
    opcion = input("Orden (m para obtener ayuda)")
    if opcion == "m" :                      
        menu_opciones()

    elif opcion =="a":
        print("toggle a bootable flag\n")
        
        respuesta =input("Numero de particion (1,2,5 defaut)")
        if respuesta != "":
            submenu1=int(respuesta)    
            if submenu1 <1 or submenu1 >5:
                print ("Value out of range\n")
            
            elif submenu1 >2 or submenu1 <5:
                print("the bootable flag on portition is disabled now\n")

            else:
                print ("the bootable flag on portition is enabled now\n")
        else:
            print("the bootable flag on portition is disabled now\n")
            
    elif opcion =="b":
        print("edit nested BSD disklabel\n")
        funcion_b()
        while True:            
            etiqueta=input("Do you want to create a BSD disklabel? [Y]es/[N]o:")
            if etiqueta=="Y":
                print("There is no *BSD partition on /dev/sda.\n")
                break
            elif etiqueta =="N":
                break
            else:
                pass            
        
        
    elif opcion =="c":
        print ("toggle the dos compatibility\n")
        print("DOS Compatibility flag is set (DEPRECATED!)\n")
    
    elif opcion =="d":
        print ("delete a partition\n")
        while True:
            respuesta1=input("Número de partición (1,2,5, default 5):")
            if respuesta1!="":            
                submenu2=int(respuesta1)
                if submenu2 >1 or submenu2 >5:
                    print("value out of range\n")
                    pass
                else:
                    print("La Particion ha sido borrada\n")
                    break
                    
            else:
                print("La particion 5 ha sido borrada")
                break
            
    elif opcion =="F":
        funcion_F()
    elif opcion =="l":
        print ("list know partitio types\n")
        funcion_l()
        
    elif opcion =="n":
        funcion_n()
       
    elif opcion =="p":
        funcion_p()

    elif opcion =="t":      
        print ("change partition type\n")
        
        while True:
            respuesta3 =input("Numero de particion (1,2,5 defaut):")
            if respuesta3 != "":
                submenu3=int(respuesta3)    
                if submenu3 <1 or submenu3 >5:
                    print("value out of rangue")
                    
                elif submenu3==3 or submenu3==4:
                    print("partition does not exit yet!")
                    break

                
                else:
                    while True:
                        tipo=input("Hex code (type L to list all codes):")
                        if tipo!="":                          
                            if tipo=="L":
                                funcion_L()
                            else:
                                parti=int(tipo)
                                if parti < 0 or parti >255:
                                    print("changed type of partition to desconocido. ")
                                else:
                                    print("tipo particion cambiado\n")
                                break
                        else:
                            while True:
                                tipo=input("Hex code (type L to list all codes):")
                                if tipo =="":
                                    pass
                                else:
                                    parti=int(tipo)
                                    if parti < 0 or parti >255:
                                        print("changed type of partition to desconocido. ")
                                    else:
                                        print("tipo particion cambiado\n")
                                    break
                                
                            
                        break
                    break
                                
                                    
                                 
                      
                        
                    
            else:
                while True:
                    tipo=input("Hex code (type L to list all codes):")
                    if tipo =="":
                        pass
                    else:
                        parti=int(tipo)
                        if parti < 0 or parti >255:
                            print("changed type of partition to desconocido. ")
                        else:
                            print("tipo particion cambiado\n")
                            break
                        break

                break
            

                         
                    
                    
        
       

    elif opcion =="v":
        funcion_v()

    elif opcion =="i":
        print ("print information about a partition\n")
        respuesta4=input("Número de partición (1,2, 5, default 5):")
        if respuesta4!="":
            if respuesta4 == "1":
               funcion_i1()
           
            elif respuesta4 == "2":
                funcion_i2()
            
            elif respuesta4 == "3" or respuesta4 == "4":
                print ("Partition does nor exist yet!\n")
            
            elif respuesta4 =="5":
                funcion_i5()
            
            else:
                print("value out of range")
        elif respuesta4=="":
            funcion_i5()
        else:
            print("unknown command")
    
    elif opcion =="u":
        print ("change display/entry units\n")
        print ("Changing display/entry units to cylinders (DEPRECATED!).\n")
        
    elif opcion == "x":     
        print("extra functionality (experts only)\n")
        
        while True:
            extra = input("Orden avanzada (m para obtener ayuda)")
            
            if extra =="m":
                funcion_xm()
            
            elif extra =="b":
                print("move beginning of dara in a partition\n")
                move = input("numero de particion (1,2,5, defaut 5)")
                if move !="":
                    print("Nuevo principio de datos (1-999423, default 2048):\n")
                    if move =="1" or move == "2" or move =="5":
                        print ("Nuevo principio de datos (1-999423, default 2048):\n")
                    
                    else:
                        print ("no data area.\n")
                else:
                    print ("Nuevo principio de datos (1-999423, default 2048):\n")
            
            elif extra =="i":
                           print("change the disk indentifier\n")
                           nombre=input("Enter the new disk identifier:")
                           
            elif extra =="c":
                print("change numer of cylinders\n")
                print ("Número de cilindros (1-1048576, default 1305):\n")
            
            elif extra =="h":
                print("change numer of headss\n")
                
            elif extra =="s":
                print("change number of sectors/track\n")
                
            elif extra =="p":
                funcion_xp()
            
            elif extra =="v":
                funcion_xv()
                
            elif extra =="d":
                funcion_xd()
                
            elif extra =="D":
                funcion_xD()
                
            elif extra =="f":
                print("fix the partitions order\n")
                print("Nothing to do. Ordering is correct already.\n")
                
            elif extra=="r":
                print("return to main menu\n")
                break
            
            elif extra =="q":
                funcion_quit()
                break
            
            else:
               print("unknown command\n")
                
    elif opcion =="I":
        funcion_I()
        
    elif opcion =="O":
        funcion_O()

    elif opcion =="g":
        print("Created a new GPT disklabel (GUID: 6BF58704-EF1D-443A-A26C-B62EE66BAEA9).\n")

    elif opcion =="G":
        print("Created a new SGI (IRIX) disklabel (GUID: 6BF58704-EF1D-443A-A26C-B62EE66BAEA9).\n")

    elif opcion =="o":
        print("Created a new DOS disklabel (GUID: 6BF58704-EF1D-443A-A26C-B62EE66BAEA9).\n")

    elif opcion =="s":
        print("Created a new Sun disklabel (GUID: 6BF58704-EF1D-443A-A26C-B62EE66BAEA9).\n")

    elif opcion =="w":
        funcion_w()
        break

    elif opcion =="q":
        funcion_quit()
        break
    
    else:
        print("unknown command\n")
        
        
        
